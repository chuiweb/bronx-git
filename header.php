<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link type="text/css" rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/main.css">
	<link type="text/css" rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/font.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/animate.min.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/owl.theme.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/owl.theme.default.css">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo bloginfo("template_url"); ?>/img/favicon.png' />
	
	<script src="<?php echo bloginfo("template_url"); ?>/js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/owl.carousel.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/smooth-scroll.js"></script>	
	<script src="<?php echo bloginfo("template_url"); ?>/js/wow.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/rellax.min.js"></script>	

	<?php wp_head(); ?>
</head>
<body>

	<div class="menu">
		<div class="container-fluid">
			<a href="<?php echo site_url(); ?>">
	    		<img class="logo-header logo-menor logo-branca" src="<?php echo bloginfo("template_url"); ?>/img/logo.png">
	    		<img class="logo-header logo-inteira logo-header-branca" src="<?php echo bloginfo("template_url"); ?>/img/logo-header.png">
	    		<img class="logo-header logo-menor logo-amarela" src="<?php echo bloginfo("template_url"); ?>/img/logo-amarela.png">
	    		<img class="logo-header logo-header-amarela" src="<?php echo bloginfo("template_url"); ?>/img/logo-header-amarela.png">
	    	</a>

	    	<div class="hamburger-lines">
              <span class="line line1"></span>
              <span class="line line2"></span>
              <span class="line line3"></span>
            </div>  
		</div>
	</div>

	<div class="submenu">
		<ul>
			<li><a href="<?php echo site_url(); ?>">HOME</a></li>
			<li><a class="to-premios" href="<?php echo site_url(); ?>/#premios">PRÊMIOS</a></li>
			<li class="menu-projetos">
				PROJETOS +
				<div class="itens-projetos">
					<?php
						global $post;

					    $args = array(
					        'post_type' => 'cases',
					        'posts_per_page' => -1,
					        'order'	=>	'ASC'
					    );

					    $post_query = new WP_Query($args);
						if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
					?>
						<a class="<?php echo $post->post_name; ?>" href="<?php echo site_url(); ?>/#<?php echo $post->post_name; ?>"><?php the_title(); ?></a>

						<script type="text/javascript">
							jQuery(".to-<?php echo $post->post_name; ?>").click(function() {
							    jQuery('html, body').animate({
							        scrollTop: jQuery("#<?php echo $post->post_name; ?>").offset().top
							    }, 1000);
							});
						</script>
					<?php } } wp_reset_postdata(); ?>
				</div>
			</li>
			<li><a class="to-contato" href="<?php echo site_url(); ?>/#contato">CONTATO</a></li>
		</ul>
	</div>
	




