<?php get_header(); ?>

	<div class="banner-principal">
		<div class="container">
			<h1>Welcome<br> to the<br> anywhere<br> agency.</h1>
			<img class="flecha-topo rellax" src="<?php echo bloginfo("template_url"); ?>/img/flecha-topo.png">
		</div>
	</div>

	<div class="sobre">
		<div class="container">
			<div class="col-xs-12 col-md-1"></div>
			<div class="col-xs-12 col-md-5">
				<img class="img-sobre rellax" data-rellax-percentage="0.5" data-rellax-speed="-1" src="<?php echo bloginfo("template_url"); ?>/img/gif-bronx.gif">
			</div>
			<div class="col-xs-12 col-md-5">
				<div class="texto-sobre">
					<?php the_field('texto_sobre', 5); ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-1"></div>
		</div>
	</div>

	<div class="premios" id="premios">
		<div class="chamada-premios">
			<div class="container-fluid">
				<img class="ag" src="<?php echo bloginfo("template_url"); ?>/img/premios/ag.png">
				<img class="logo-premios rellax" data-rellax-percentage="0.5" src="<?php echo bloginfo("template_url"); ?>/img/premios/logo-premios.png">
				<img class="anywhere-agency" src="<?php echo bloginfo("template_url"); ?>/img/premios/anywhere-agency.png">
				<img class="retangulo rellax" data-rellax-percentage="0.5" data-rellax-speed="2" src="<?php echo bloginfo("template_url"); ?>/img/premios/retangulo.png">
				<div class="col-xs-12 col-md-1"></div>
				<div class="col-xs-12 col-md-8 col-titulo">
					<h2>Recognized <img class="hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/premios/arrows-premios.png"> anywhere.</h2>
				</div>
			</div>
			<div class="container-fluid">
				<div class="col-xs-12 col-md-4"></div>
				<div class="col-xs-12 col-md-4">
					<p>Começamos nossa trajetória em Curitiba, mais precisamente em 1993. Nosso reconhecimento começou no bairro onde a agência ficava: os vizinhos achavam bonito o totem em frente à sede. Mas logo começamos a nos destacar no mercado de Curitiba, conquistando a maior parte dos prêmios de criação regionais. Logo depois conquistamos reconhecimento no anuário do CCSP, que até hoje reúne os trabalhos mais criativos do país e daí pra frente passamos a frequentar premiações internacionais e ser presença constante em publicações impressas e online de alcance mundial. 
					<strong>VEJA ALGUMAS DELAS:</strong></p>
					<img class="flechas-premios rellax" data-rellax-percentage="0.5" data-rellax-speed="-1" src="<?php echo bloginfo("template_url"); ?>/img/premios/flechas-premios.png">
				</div>
				<img class="asterisco-premios rellax" data-rellax-percentage="0.5" data-rellax-speed="-2" src="<?php echo bloginfo("template_url"); ?>/img/premios/asterisco-premios.png">
			</div>
		</div>

		<div class="lista-premios hidden-xs">
			<img class="ag" src="<?php echo bloginfo("template_url"); ?>/img/premios/ag.png">
			<div class="container col-lista">
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/premio-colunistas.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO COLUNISTAS PR</strong> - AGÊNCIA DO ANO E MAIS PREMIADA DIVERSAS VEZES.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/grpcom.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO GRPCOM</strong> - AGÊNCIA DO ANO E MAIS PREMIADA POR 2 EDIÇÕES.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/anj.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO ANJ</strong> - GRAND PRIX E DIVERSOS PRÊMIOS OURO.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/central-outdoor.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO CENTRAL DE OUTDOOR PR E BRASIL</strong> - DIVERSOS GRAND PRIX E MEDALHAS DIVERSAS. VEZES.</h5>
					</div>
				</div>
			</div>
			<div class="container col-lista">
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/top-mkt.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>TOP DE MARKETING ADVB.</strong> - PARA 2 CASES DE CLIENTES NOSSOS, FEITOS A 4 MÃOS.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/aberje.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO ABERJE DE COMUNICAÇÃO EMPRESARIAL</strong> - PARA O GRUPO MARISTA.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/abemd.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO ABEMD</strong> - OURO EM MARKETING DIRETO PARA O HSBC.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/profissionais-ano.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>PRÊMIO PROFISSIONAIS DO ANO DA REDE GLOBO SUL</strong> - CAMPEÃ E DIVERSAS VEZES FINALISTA.</h5>
					</div>
				</div>
			</div>
			<div class="container col-lista">
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/ccsp.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>ANUÁRIO DO CCSP</strong> - PRESENTE COM FILMES E PRINTS EM EDIÇÕES DIFERENTES.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/fiap.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>FIAP</strong> - PRATA NO FESTIVAL IBERO AMERICANO DE PUBLICIDADE.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/newyork-festival.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>THE NEW YORK FESTIVALS</strong> - ANÚNCIO PUBLICADO NO ANUÁRIO.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/ccpr.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>CLUBE DE CRIAÇÃO DO PARANÁ</strong> - DIVERSAS MEDALHAS E PRÊMIOS ANUÁRIOS DESDE A EDIÇÃO 01.</h5>
					</div>
				</div>
			</div>
			<div class="container col-lista">
				<div class="col-xs-12 col-md-6 col-lg-3 hidden-md"></div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/best-ads.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>BESTADSONTV</strong>  - MAIS DE 100 TRABALHOS PUBLICADOS EM SITES INTERNACIONAIS DIFERENTES, INCLUINDO 10 “BEST OF THE WEEK”.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3">
					<div class="col-xs-5">
						<img class="logo-premios" src="<?php echo bloginfo("template_url"); ?>/img/premios/archive.png">
					</div>
					<div class="col-xs-7 col-texto">
						<h5><strong>LÜRZER'S ARCHIVE</strong> - MAIS DE 15 PUBLICAÇÕES, INCLUINDO “BEST OF THE WEEK”.</h5>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-3"></div>
			</div>
			<img class="rellax" data-rellax-percentage="0.5" data-rellax-speed="1" src="<?php echo bloginfo("template_url"); ?>/img/premios/arrows-premios.png">
			<img class="asterisco-premios rellax" data-rellax-percentage="0.5" data-rellax-speed="2" src="<?php echo bloginfo("template_url"); ?>/img/premios/asterisco2-premios.png">
		</div>

		<div class="lista-premios hidden-sm hidden-md hidden-lg">
			<img class="ag" src="<?php echo bloginfo("template_url"); ?>/img/premios/ag.png">
			<div id="slider-premios-mobile" class="owl-carousel owl-theme">
				<div class="item">
					<img src="<?php echo bloginfo("template_url"); ?>/img/premios/premios-mobile-1.png">
				</div>
				<div class="item">
					<img src="<?php echo bloginfo("template_url"); ?>/img/premios/premios-mobile-2.png">
				</div>
				<div class="item">
					<img src="<?php echo bloginfo("template_url"); ?>/img/premios/premios-mobile-3.png">
				</div>
				<div class="item">
					<img src="<?php echo bloginfo("template_url"); ?>/img/premios/premios-mobile-4.png">
				</div>
			</div>
		</div>
	</div>

	<div class="projetos">
		<div class="chamada-projetos">
			<img class="ag" src="<?php echo bloginfo("template_url"); ?>/img/ag.png">
			<div class="container">
				<h1>Anywhere<br> ´´´´ your idea fits.</h1>
				<img class="linha" src="<?php echo bloginfo("template_url"); ?>/img/linha.png">
				<div class="col-xs-12 col-md-6">
					<p>Assim como não acreditamos em fronteiras físicas como obstáculo na relação agência-cliente, somos totalmente a favor de campanhas pensadas para qualquer meio onde funcionem melhor. Online, offline, com cross entre o digital e o off, em forma de ativação, mídia indoor. O lugar não pode ser um limitador para nada, muito menos para as boas ideias.</p>
					<p>|¦¦||¦¦||¦¦¦||¦¦||¦¦|</p>
				</div>
				<img class="arrow-idea rellax hidden-xs" data-rellax-percentage="0.5" src="<?php echo bloginfo("template_url"); ?>/img/arrow-idea.png">
				<img class="arrow-idea hidden-sm hidden-md hidden-lg" src="<?php echo bloginfo("template_url"); ?>/img/arrow-idea.png">
			</div>
		</div>

		<?php
			global $post;

			$count = 1;
		    $args = array(
		        'post_type' => 'cases',
		        'posts_per_page' => -1,
		        'order'	=>	'ASC'
		    );

		    $post_query = new WP_Query($args);
			if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
		?>
		<div class="slider-projetos" id="<?php echo $post->post_name; ?>">
			<img class="open-text open-text-<?php echo $count; ?>" src="<?php echo bloginfo("template_url"); ?>/img/open-text.png">
			<?php $selo = get_field("selo_cases"); ?>
			<img class="selo-slider" src="<?php echo $selo['sizes']['large']; ?>">

			<?php if(get_field("imagens_cases")) { ?>
			<div id="banner-projetos-<?php echo $count; ?>" class="owl-carousel owl-theme">
				<?php
				    $images = acf_photo_gallery('imagens_cases', $post->ID);
				    if( count($images) ):
				        foreach($images as $image):
				            $full_image_url= $image['full_image_url'];
				?>
				<div class="item">
					<img src="<?php echo $full_image_url; ?>">
				</div>
				<?php endforeach; endif; ?>
			</div>
			<?php } elseif (get_field("video_cases")) { ?>
			<div class="video-projetos">
				<?php echo the_field("video_cases"); ?>
			</div>
			<?php } ?>

			<div class="box-projeto">
				<?php echo the_field('descricao_cases'); ?>
				<h5 class="cliente"><?php echo the_field('cliente_cases'); ?></h5>
				<h5 class="ano"><?php echo the_field('ano_cases'); ?></h5>
			</div>
		</div>
		<?php $count++; } } wp_reset_postdata(); ?>

	</div>

	<div class="parceiros">
		<div class="chamada-parceiros">
			<img class="ag-white hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/ag-white.png">
			<div class="container-fluid">
				<img class="logo-partners hidden-sm hidden-md hidden-lg" src="<?php echo bloginfo("template_url"); ?>/img/logo-partners-mobile.png">
				<div class="col-xs-12 col-md-8">
					<div class="col-xs-12 col-md-2"></div>
					<div class="col-xs-12 col-md-9">
						<h1>Partners <span>anywhere.</span></h1>
						<p>Hoje a Bronx tem projetos já realizados para clientes como a multinacional Enaex, com materiais exportados para o Chile, França, Austrália e todas as sedes do grupo. Tem trabalhos entregues para o agronegócio, para a ADAMA, empresa global presente em 100 países e com sede em Londrina. Durante anos trabalhou para a Unimed Brasil, com sede em São Paulo, criando campanhas distribuídas no país inteiro. E por mais de 15 anos vem atendendo a singular local da empresa, a Unimed Curitiba. Esses são apenas alguns exemplos de grandes marcas de diversos lugares que confiaram sua comunicação à Bronx.
						<span>||||||||||||||||||||</span></p>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<img class="logo-partners hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/logo-partners.png">
				</div>
				<img class="arrow-partners hidden-sm hidden-md hidden-lg" src="<?php echo bloginfo("template_url"); ?>/img/arrow-partners.png">
			</div>
		</div>

		<div class="lista-parceiros">
			<div class="container hidden-xs">
				<div class="col-xs-12 col-md-2"></div>
				<div class="col-xs-12 col-md-8">
					<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-01.png">
					<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-02.png">
					<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-03.png">
					<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-04.png">
					<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-05.png">
				</div>
				<div class="col-xs-12 col-md-2"></div>
			</div>
			<div class="container hidden-sm hidden-md hidden-lg">
				<img src="<?php echo bloginfo("template_url"); ?>/img/clientes-mobile.png">
			</div>
		</div>
	</div>

	<div class="contato" id="contato">
		<div class="container-fluid">
			<div class="col-xs-12 col-md-6 col-contato formulario">
				<h2>Fale conosco:</h2>
				<?php echo do_shortcode( '[contact-form-7 id="10" title="Formulário de contato"]' ); ?>
			</div>
			<div class="col-xs-12 col-md-6 col-contato infos">
				<h4>Contato</h4>
				<p>+55 (41) 3122-4900</p>
				<p>+55 (41) 99654-1010 | Alexandre</p>
				<p>+55 (41) 99654-3030 | Cláudio</p>

				<h5><strong>CURITIBA</strong>
				R. Humberto Carta, 96 - Hugo Lange, Curitiba - PR - 80040-150</h5>

				<img class="borda-footer" src="<?php echo bloginfo("template_url"); ?>/img/borda-footer.png">

				<img class="logo-footer" src="<?php echo bloginfo("template_url"); ?>/img/logo-footer.png">
				<p class="copyright hidden-xs">© 2022 BRONX.ag Todos Direitos Reservados.</p>
				<div class="redes-sociais">
					<ul>
						<li>
							<a href="https://www.facebook.com/bronx.comunicacao/" target="_blank">
								<i class="fab fa-facebook-f"></i>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/bronx.ag/" target="_blank">
								<i class="fab fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/company/bronx/mycompany/" target="_blank">
								<i class="fab fa-linkedin"></i>
							</a>
						</li>
					</ul>
				</div>
				<p class="copyright hidden-sm hidden-md hidden-lg">© 2022 BRONX.ag Todos Direitos Reservados.</p>
			</div>
		</div>
	</div>

<?php get_footer(); ?>