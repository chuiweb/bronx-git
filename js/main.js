$(document).ready(function(){

	var rellax = new Rellax('.rellax');

	$("#banner-projetos-1, #banner-projetos-2, #banner-projetos-3, #banner-projetos-4, #banner-projetos-5, #banner-projetos-6, #banner-projetos-7, #banner-projetos-8, #banner-projetos-9, #banner-projetos-10").owlCarousel({
		nav : false,
		dots : true,
		items : 1,
		autoplay : true,
		loop : true,
		autoplayTimeout:10000
  	});

  	$("#slider-premios-mobile").owlCarousel({
		nav : false,
		dots : true,
		items : 1,
		autoplay : true,
		loop : true,
		autoplayTimeout:10000
  	});

  	$(".hamburger-lines").click(function(){
  		$(this).toggleClass("clicked");
  		$(".submenu").toggleClass("open");
  		$(".menu .logo-header").toggleClass("logo-change");
  		$(".menu.menu-scroll").toggleClass("bg-black");
  	});

  	$(".menu-projetos").click(function(){
  		$(".itens-projetos").toggleClass("itens-show");
  	});

  	$(".slider-projetos .open-text").click(function(){
  		$(".slider-projetos .box-projeto").toggleClass("show-box");
  		$(this).toggleClass("rotate-icon");
  	});

  	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();
	    var banner = $(".banner-principal").height();

	    if (scroll >= banner) {
	        $(".menu .logo-amarela, .menu .logo-header-amarela").addClass("menu-scroll");
	        $(".menu .logo-branca, .menu .logo-header-branca").addClass("hidden-logos");
	        $(".hamburger-lines .line").css("background", "#FDC42B");
	    }
	    if (scroll < banner) {
	        $(".menu .logo-amarela, .menu .logo-header-amarela").removeClass("menu-scroll");
	        $(".menu .logo-branca, .menu .logo-header-branca").removeClass("hidden-logos");
	        $(".hamburger-lines .line").css("background", "#ffffff");
	    }
	}); 

	$(".to-premios").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#premios").offset().top
	    }, 1000);
	});

	$(".to-contato").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#contato").offset().top
	    }, 1000);
	});

	$(".submenu ul li a").click(function() {
	    $(".submenu").removeClass("open");
	    $(".hamburger-lines").removeClass("clicked");
  		$(".menu .logo-header").removeClass("logo-change");
  		$(".menu.menu-scroll").removeClass("bg-black");
	});


});